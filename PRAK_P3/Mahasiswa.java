package PRAK_P3;

import java.util.Scanner;

public class Mahasiswa {
    public String npm, nama, tempatPkl;
    public Nilai nilai;

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTempatPkl() {
        return tempatPkl;
    }

    public void setTempatPkl(String tempatPkl) {
        this.tempatPkl = tempatPkl;
    }

    public Nilai getNilai() {
        return nilai;
    }

    public void setNilai(Nilai nilai) {
        this.nilai = nilai;
    }

    public void _cetak() {
        System.out.println("NIM\t\t: " + getNpm());
        System.out.println("Nama\t\t: " + getNama());
        System.out.println("Tempat PKL\t: " + getTempatPkl());
        System.out.println("Nilai Akhir\t: " + nilai._hitungNilaiAkhir());
    }

    public Mahasiswa(String npm, String nama, String tempatPkl, Nilai nilai) {
        this.npm = npm;
        this.nama = nama;
        this.tempatPkl = tempatPkl;
        this.nilai = nilai;
    }
}

class Nilai {
    private int prak, lap, ujian, akhir;

    public int getPrak() {
        return prak;
    }

    public void setPrak(int prak) {
        this.prak = prak;
    }

    public int getLap() {
        return lap;
    }

    public void setLap(int lap) {
        this.lap = lap;
    }

    public int getUjian() {
        return ujian;
    }

    public void setUjian(int ujian) {
        this.ujian = ujian;
    }

    public int getAkhir() {
        return akhir;
    }

    public void setAkhir(int akhir) {
        this.akhir = akhir;
    }

    public int _hitungNilaiAkhir() {
        return akhir = ((prak * 6) + (lap * 2) + (ujian * 2)) / 10;
    }

    public Nilai(int prak, int lap, int ujian) {
        this.prak = prak;
        this.lap = lap;
        this.ujian = ujian;
    }
}

class olahData {
    public Scanner input = new Scanner(System.in);
    public Mahasiswa[] mhs;

    public void inputData() {
        String Npm, Nama, TempatPkl;
        Nilai nilai;
        int prak, lap, ujian, N;
        System.out.print("Jumlah Mahasiswa : ");
        N = input.nextInt();
        mhs = new Mahasiswa[N];
        for (int i = 0; i < mhs.length; i++) {
            System.out.println();
            System.out.println("Data Mahasiswa ke - " + (i + 1));
            System.out.print("Masukan NIM\t: ");
            Npm = input.next();
            input.nextLine();
            System.out.print("Masukan Nama\t: ");
            Nama = input.nextLine();
            System.out.print("Tempat PKL\t: ");
            TempatPkl = input.nextLine();
            System.out.print("Masukan Nilai Praktikum\t: ");
            prak = input.nextInt();
            System.out.print("Masukan Nilai Lapangan\t: ");
            lap = input.nextInt();
            System.out.print("Masukan Nilai Ujian\t: ");
            ujian = input.nextInt();
            nilai = new Nilai(prak, lap, ujian);
            mhs[i] = new Mahasiswa(Npm, Nama, TempatPkl, nilai);
        }
        System.out.println("Data tersimpan.");
    }

    public void _sorting() {
        Mahasiswa tmp;
        for (int i = 0; i < mhs.length - 1; i++) {
            System.out.println();
            for (int j = i + 1; j < mhs.length; j++) {
                if (mhs[i].tempatPkl.compareTo(mhs[j].tempatPkl) > 0) {
                    tmp = mhs[i];
                    mhs[i] = mhs[j];
                    mhs[j] = tmp;
                }
            }
        }
    }

    public int _searchByNpm(String masukan) {
        int pos = -1;
        for (int i = 0; i < mhs.length; i++) {
            if (mhs[i].npm.equals(masukan)) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    public void _menuCetakByNpm() {
        String npm;
        int pos;
        System.out.println("\nMenu cari data by NIM");
        System.out.print("Masukan NIM\t: ");
        npm = input.next();
        pos = _searchByNpm(npm);

        if (pos < 0) {
            System.out.println("\nData tidak ada.");
        } else {
            System.out.println("\nMenampilkan data Mahasiswa: ");
            mhs[pos]._cetak();
        }
    }

    public void cetakData() {
        for (int i = 0; i < mhs.length; i++) {
            mhs[i]._cetak();
            System.out.println();
        }
    }

    public static void main(String[] args) {
        olahData od = new olahData();
        int n;
        od.inputData();
        od._sorting();
        System.out.println();
        System.out.println("Pilihan menu: \n1. Cari data by Nim \n2. Cetak Semua Data");
        n = od.input.nextInt();
        switch (n) {
        case 1:
            od._menuCetakByNpm();
            break;
        case 2:
            System.out.println("\nCetak Mahasiswa : ");
            od.cetakData();
            break;
        default:
            System.out.println("error an occured");
        }
    }
}