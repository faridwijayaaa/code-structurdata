package Searching;

import java.util.Scanner;

public class SeqSearch {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] x = { 20, 50, 10, 30, 90, 60, 70, 80, 40, 100 };
        int i, y;
        boolean found = false;
        System.out.print("Nilai yang dicari : ");
        y = input.nextInt();
        i = 0;
        while ((i < x.length) && (!found)) {
            if (x[i] == y) {
                found = true;
            } else {
                i++;
            }
        }

        if (found) {
            System.out.println(y + ", ditemukan pada index array ke-" + i);
        } else {
            System.out.println(y + ", tidak ada dalam Array tersebut.");
        }
    }
}