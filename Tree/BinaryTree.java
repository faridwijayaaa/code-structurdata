package Tree;

import java.util.Scanner;

class TreeNode {
    int data;
    TreeNode left;
    TreeNode right;

    public TreeNode(int data) {
        this.data = data;
    }
}

public class BinaryTree {
    TreeNode root;

    boolean isEmpty() {
        return (root == null);
    }

    // Method menambah data / Insert data
    void isInsert(TreeNode input) {
        if (isEmpty()) {
            root = input;
        } else {
            // Menentukan parent yang sesuai (Kiri / Kanan)
            TreeNode current = root;
            TreeNode parent = null;
            boolean left = true;
            while (current != null) {
                parent = current;
                // Kalau data yang diinputkan lebih besar, maka akan bergerak ke kanan
                if (current.data < input.data) {
                    current = current.right;
                    left = false;
                } else if (current.data > input.data) {
                    current = current.left;
                    left = true;
                } else {
                    System.out.println("Data " + input.data + " sudah ada.");
                    break;
                }
            }
            // Hubungan ke Parent
            if (left) {
                parent.left = input;
            } else {
                parent.right = input;
            }
        }
    }

    // Pre - Order
    void preOrder(TreeNode akar) {
        if (akar != null) {
            System.out.print(akar.data + " ");
            preOrder(akar.left);
            preOrder(akar.right);
        }
    }

    // in - Order
    void inOrder(TreeNode akar) {
        if (akar != null) {
            inOrder(akar.left);
            System.out.print(akar.data + " ");
            inOrder(akar.right);
        }
    }

    // Post Order
    void postOrder(TreeNode akar) {
        if (akar != null) {
            postOrder(akar.left);
            postOrder(akar.right);
            System.out.print(akar.data + " ");
        }
    }

    // Method Search data
    TreeNode isSearch(int key) {
        TreeNode node = null;
        TreeNode current = root;
        // Melakukan pencarian selama current bukan null
        while (current != null) {
            if (current.data == key) {
                return current;
            } else {
                if (current.data > key) {
                    current = current.left;
                } else {
                    current = current.right;
                }
            }
        }
        return node;
    }

    // Menghitung jumlah total node . .
    int isCount(TreeNode xNode) {
        if (xNode == null) {
            return 0;
        } else {
            return isCount(xNode.left) + isCount(xNode.right) + 1;
        }
    }

    // Menghitung kedalaman node . .
    int isHeight(TreeNode xNode) {
        if (xNode == null) {
            return -1;
        } else {
            int x = isHeight(xNode.left), y = isHeight(xNode.right);
            if (x > y) {
                return x + 1;
            } else {
                return y + 1;
            }
        }
    }

    // Mencari nilai terkecil pada tree
    int findMin(TreeNode xNode) {
        if (isEmpty()) {
            return 0;
        } else {
            if (xNode.left == null) {
                return xNode.data;
            } else {
                return findMin(xNode.left);
            }
        }
    }

    // Mencari nilai terbesar pada tree
    int findMax(TreeNode xNode) {
        if (isEmpty()) {
            return 0;
        } else {
            if (xNode.right == null) {
                return xNode.data;
            } else {
                return findMax(xNode.right);
            }
        }
    }

    // Mencari Leaf
    void leaf(TreeNode xNode) {
        if (isEmpty()) {
            System.out.println("KOSONG!");
        } else {
            if (xNode.left == null && xNode.right == null) {
                System.out.println(xNode.data);
            } else {
                if (xNode.left != null) {
                    leaf(xNode.left);
                }
                if (xNode.right != null) {
                    leaf(xNode.right);
                }
            }
        }
    }

    TreeNode isDelete(TreeNode Xnode, int key) {
        if (Xnode == null) {
            return Xnode;
        }

        if (key < Xnode.data) {
            Xnode.left = isDelete(Xnode.left, key);
        } else if (key > Xnode.data) {
            Xnode.right = isDelete(Xnode.right, key);
        } else {
            if (Xnode.left == null) {
                return Xnode.right;
            } else if (Xnode.right == null) {
                return Xnode.left;
            }
            Xnode.data = findMin(Xnode.right);
            Xnode.right = isDelete(Xnode.right, Xnode.data);
        }
        return Xnode;
    }

    void isDelete(int key) {
        isDelete(root, key);
    }

    void preOrder() {
        preOrder(root);
    }

    void inOrder() {
        inOrder(root);
    }

    void postOrder() {
        postOrder(root);
    }

    void isCount() {
        System.out.println(isCount(root));
    }

    void isHeight() {
        System.out.println(isHeight(root));
    }

    void findMin() {
        System.out.println(findMin(root));
    }

    void findMax() {
        System.out.println(findMax(root));
    }

    void leaf() {
        leaf(root);
    }
}

class BinaryTreeApp {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        TreeNode node;
        Scanner input = new Scanner(System.in);
        int pil = 1, data;
        System.out.println("\n1. Insert. \n2. Search. \n3. Menghitung total node. \n4. Menghitung kedalaman node." +
                " \n5. Mencari nilai terkecil pada node. \n6. Mencari nilai terbesar pada node." +
                " \n7. Lihat leaf. \n8. Cetak. \n9. Deleted. \n0. Exit.");

        while (pil < 10 && pil > 10 || pil != 0) {
            System.out.print("\nPilihan : ");
            pil = input.nextInt();
            switch (pil) {
                case 1:
                    System.out.print("Masukan node : ");
                    node = new TreeNode(data = input.nextInt());
                    tree.isInsert(node);
                    break;
                case 2:
                    System.out.print("Masukan node yang dicari: ");
                    data = input.nextInt();
                    if (tree.isSearch(data) != null) {
                        System.out.println("node " + data + " ditemukan.");
                    } else {
                        System.out.println("node tidak ditemukan");
                    }
                    break;
                case 3:
                    System.out.print("Count: ");
                    tree.isCount();
                    break;
                case 4:
                    System.out.print("Kedalaman: ");
                    tree.isHeight();
                    break;
                case 5:
                    System.out.print("Node terkecil: ");
                    tree.findMin();
                    break;
                case 6:
                    System.out.print("Node terbesar:");
                    tree.findMax();
                    break;
                case 7:
                    System.out.print("Leaf: ");
                    tree.leaf();
                    break;
                case 8:
                    System.out.print("Traversal dengan Pre - Order: ");
                    tree.preOrder();
                    System.out.println();
                    System.out.print("Traversal dengan In - Order: ");
                    tree.inOrder();
                    System.out.println();
                    System.out.print("Traversal dengan Post - Order: ");
                    tree.postOrder();
                    System.out.println();
                    break;
                case 9:
                    System.out.print("Masukan node yang mau dihapus: ");
                    data = input.nextInt();
                    tree.isDelete(data);

                    break;
                case 0:
                    System.out.println("Keluar, Terima kasih . .");
                    System.exit(0);
                default:
                    System.out.println("Keluar Otomatis . .");
            }
        }
    }
}
