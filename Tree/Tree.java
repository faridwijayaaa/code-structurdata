package Tree;

import java.util.Scanner;

class node {
    node left;
    node right;
    int value;

    public void input(int a) {
        value = a;
    }
}

public class Tree {
    static Scanner in = new Scanner(System.in);

    public void insert(node root, int databaru) {
        if (databaru < root.value) {
            if (root.left != null)
                insert(root.left, databaru);
            else {
                root.left = new node();
                root.left.input(databaru);
                System.out.println(databaru + " di kiri  " + root.value);
            }
        } else if (databaru > root.value) {
            if (root.right != null)
                insert(root.right, databaru);
            else {
                root.right = new node();
                root.right.input(databaru);
                System.out.println(databaru + " di kanan " + root.value);
            }
        }
    }

    public node Deleted(node Xnode, int key) {
        if (Xnode == null) {
            return Xnode;
        }

        if (key < Xnode.value) {
            Xnode.left = Deleted(Xnode.left, key);
        } else if (key > Xnode.value) {
            Xnode.right = Deleted(Xnode.right, key);
        } else {
            if (Xnode.left == null) {
                return Xnode.right;
            } else if (Xnode.right == null) {
                return Xnode.left;
            }
            // Xnode.data = findMin(Xnode.right);
            Xnode.right = Deleted(Xnode.right, Xnode.value);
        }
        return Xnode;
    }

    public void view(node data) {
        System.out.println(" ");
        System.out.print("Pre Order   : ");
        preOrder(data);
        System.out.println(" ");
        System.out.print("In Order    : ");
        inOrder(data);
        System.out.println(" ");
        System.out.print("Post Order  : ");
        postOrder(data);
        System.out.println(" ");
    }

    public void preOrder(node pre) {
        if (pre != null) {
            System.out.print(pre.value + " ");
            preOrder(pre.left);
            preOrder(pre.right);
        }
    }

    public void inOrder(node in) {
        if (in != null) {
            inOrder(in.left);
            System.out.print(in.value + " ");
            inOrder(in.right);
        }
    }

    public void postOrder(node post) {
        if (post != null) {
            postOrder(post.left);
            postOrder(post.right);
            System.out.print(post.value + " ");
        }
    }

    public static void main(String[] args) {
        Tree tr = new Tree();
        node root = new node();
        int menu = 1;
        // int r = 1;
        // int y = 1;
        // int newdata[] = new int[99];
        int data;

        while (menu != 4) {
            System.out.println("----- PROGRAM TREE -----");
            System.out.print("1. Tambahkan Data Baru\n2. View All\n3. Delete\n4. Keluar\nMasukkan pilihan : ");
            menu = tr.in.nextInt();
            if (menu == 1) {
                System.out.print("Masukkan Angka : ");
                data = tr.in.nextInt();
                tr.insert(root, data);
                // newdata[y] = tr.in.nextInt();
                // if (r == 1) {
                // root.input(newdata[y]);
                // r--;
                // } else
                // tr.insert(root, newdata[y]);
            } else if (menu == 2) {
                // for (int x = 1; x < y; x++) {
                // System.out.print(newdata[x] + " ");
                // }
                System.out.print("---------------");
                tr.view(root);
            } else if (menu == 3) {
                System.out.print("Masukan Angka : ");
                data = tr.in.nextInt();
                tr.Deleted(root, data);
            } else if (menu == 4)
                System.out.println("Thank You :) ");
            else
                System.out.println("Salah");
            System.out.println(" ");
            // y += 1;
        }
    }
}
