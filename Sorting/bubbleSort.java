package Sorting;

public class bubbleSort {
    public static void main(String[] args) {
        int[] data = { 5, 34, 32, 25, 15, 43, 22, 2 };
        int tmp;

        System.out.println("Data Sebelum diurutukan :");
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " ");
        }

        for (int i = 0; i < data.length - 1; i++) {
            for (int j = 0; j < data.length - 1; j++) {
                if (data[j] > data[j + 1]) {
                    tmp = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = tmp;
                }
            }
            System.out.println();
            for (int k = 0; k < data.length; k++) {
                System.out.print(data[k] + " ");
            }
        }
        System.out.println("\nData setelah diurutkan : ");
        for (int i = 0; i < data.length; i++) {
            System.out.print(data[i] + " ");
        }
    }
}