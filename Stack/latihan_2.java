package Stack;

import java.util.Scanner;
import java.util.Stack;

public class latihan_2 {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<Integer>();
        Scanner input = new Scanner(System.in);
        String loop;
        do {
            System.out.println(
                    "Pilihan: \n1. Mengecek Full pada Stack. \n2. Mengecek Empty pada Stack. \n3. Push Data \n4. Pop Data \n5. Menghapus semua Data \n6. Cetak Semua Data");
            System.out.print("Masukan : ");
            int N = input.nextInt();
            switch (N) {
            case 1:
                break;
            case 2:
                stack.empty();
                break;
            case 3:
                System.out.print("Masukan data yang akan ditambahkan: ");
                stack.push(input.nextInt());
                break;
            case 4:
                System.out.println("Menghapus 1 Data dari atas.");
                stack.pop();
                break;
            case 5:
                System.out.println("Menghapus Semua Data.");
                stack.clear();
                break;
            default:
                System.out.println("error an occured");
            }

            System.out.println("\nApakah anda ingin mengulang lagi ? (ya / tidak)");
            loop = input.next();
            System.out.println();
        } while (loop.equals("ya"));
    }
}
