package Stack;

import java.util.Scanner;

public class latihan {
    protected int maxStack = 5;
    protected Integer[] stack = new Integer[maxStack];
    protected boolean content;

    protected boolean full() {
        for (int i = 0; i < stack.length; i++) {
            if (stack[i] != null) {
                content = true;
            } else {
                content = false;
            }
        }
        System.out.println(content);
        return content;
    }

    protected boolean empty() {
        for (int i = 0; i < stack.length; i++) {
            if (stack[0] == null) {
                content = true;
            } else {
                content = false;
                break;
            }
        }
        System.out.println(content);
        return content;
    }

    protected void push(int content) {
        for (int i = 0; i < stack.length; i++) {
            if (stack[i] == null) {
                stack[i] = content;
                break;
            }
        }
    }

    protected void pop() {
        for (int i = 0; i < stack.length; i++) {
            if (stack[i] == null) {
                stack[i - 1] = null;
                break;
            }
        }
    }

    protected void cetak() {
        for (int i = 0; i < stack.length; i++) {
            System.out.print(stack[i] + "\t");
        }
        System.out.println();
    }

    protected void clear() {
        int i = 0, j = 1;
        while (i < stack.length) {
            if (stack[i] == null) {
                stack[i - j] = null;
                j++;
                continue;
            } else {
                i++;
            }
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        latihan stack = new latihan();
        String loop;
        do {
            System.out.println(
                    "Pilihan: \n1. Mengecek Full pada Stack. \n2. Mengecek Empty pada Stack. \n3. Push Data \n4. Pop Data \n5. Menghapus semua Data \n6. Cetak Semua Data");
            System.out.print("Masukan : ");
            int N = input.nextInt();
            switch (N) {
            case 1:
                stack.full();
                break;
            case 2:
                stack.empty();
                break;
            case 3:
                System.out.print("Masukan data yang akan ditambahkan: ");
                stack.push(input.nextInt());
                break;
            case 4:
                System.out.println("Menghapus 1 Data dari atas.");
                stack.pop();
                break;
            case 5:
                System.out.println("Menghapus Semua Data.");
                stack.clear();
                break;
            case 6:
                System.out.println("Cetak semua data.");
                stack.cetak();
                break;
            default:
                System.out.println("error an occured");
            }

            System.out.println("\nApakah anda ingin mengulang lagi ? (ya / tidak)");
            loop = input.next();
            System.out.println();
        } while (loop.equals("ya"));
    }
}
